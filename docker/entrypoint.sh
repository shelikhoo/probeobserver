#!/bin/bash
pushd /bridgetest/
source ./conf/script.sh
if [ -z $SKIP_SNOWFLAKE ]
then
./probetest.sh snowflake $SITENAME
fi
./probetest.sh obfs4 $SITENAME
popd

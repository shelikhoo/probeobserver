#!/bin/bash

set -e

CASE="${1:?}"
SITE="${2:?}"

if ! [[ "$CASE" =~ ^(obfs4|snowflake)$ ]]; then
    echo 'Error, please choose a valid test type from ["obfs4"|"snowflake"]'
    exit 1
fi
if [ ! -z $UPDATE_TESTCASE ]
then
  ./util -action update -grpc-target $GRPC_SITE -grpc-authority $GRPC_AUTHORITY -test-site-name $SITE
fi
dirname="$PWD"
date=$(date -u +%Y%m%d-%H%M)
logdirname="log/$CASE/$SITE/$date"
mkdir -p "$logdirname"
pushd "$logdirname"

case $CASE in
    'obfs4')
        "$dirname/obfs4test" "$dirname/bridge_lines.txt"
        ;;
    'snowflake')
        # First test reachability of STUN servers
        "$dirname/stun-test/stun-test"
        truncate -s 0 "$dirname/snowflake_name_bridge_lines.txt"
        curl $CURL_ARG https://gitlab.torproject.org/tpo/anti-censorship/rdsys-admin/-/raw/main/conf/circumvention.json?ref_type=heads > "$dirname/snowflake_bridge_lines_orig.txt"
        cat "$dirname/snowflake_bridge_lines_orig.txt" | jq -r ".$SNOWFLAKE_BRIDGELINE_REGIONNAME.settings[]|select(.bridges.type==\"snowflake\")|.bridges.bridge_strings[]" > "$dirname/snowflake_bridge_lines.txt"

        counter=0
        while read SNOWFLAKE_BRIDGELINE; do
            export SNOWFLAKE_BRIDGELINE
            export SNOWFLAKE_BRIDGENAME=$SNOWFLAKE_BRIDGELINE_REGIONNAME-$counter
            
            counter_inner=0
            while [ $counter_inner -lt 10 ]
            do
            echo "snowflake-probe-"$SNOWFLAKE_BRIDGENAME"-"$counter_inner"-tor"","$SNOWFLAKE_BRIDGELINE >> "$dirname/snowflake_name_bridge_lines.txt"
            counter_inner=$((counter_inner+1))
            done
            "$dirname/snowflaketest"
            counter=$((counter+1))
        done <"$dirname/snowflake_bridge_lines.txt"
        ;;
esac
popd

chown -R $UID:$UID $dirname/log/
TARNAME=$dirname/log/$CASE/$SITE/$date.tar.gz
tar -czvf $TARNAME $dirname/log/$CASE/$SITE/$date
chown -R $UID:$UID $dirname/log/$CASE/$SITE/$date.tar.gz
rm -rf $dirname/log/$CASE/$SITE/$date
if [ ! -z $SUBMIT_RESULT ]
then
  
  case $CASE in
    'obfs4')
        ./util -in-bridgeline ./bridge_lines.txt -in-tar $TARNAME -out-type grpc -grpc-target $GRPC_SITE -grpc-authority $GRPC_AUTHORITY -test-site-name $SITE
        ;;
    'snowflake')
        ./util -in-bridgeline ./snowflake_name_bridge_lines.txt -in-tar $TARNAME -out-type grpc -grpc-target $GRPC_SITE -grpc-authority $GRPC_AUTHORITY -test-site-name $SITE
        ;;
  esac
fi
if [ ! -z $SUBMIT_RAW_RESULT ]
then
  ./util -in-tar $TARNAME -action uploadhashed -hashstore-target $HASHSTORE_SITE
fi
if [ ! -z $SUBMIT_RAW_RESULT_CHUNKED ]
then
  ./util -in-tar $TARNAME -action uploadhashedChunked -hashstore-target $HASHSTORE_SITE_CHUNKED
fi
if [ ! -z $RM_RESULT ]
then
rm $TARNAME
fi

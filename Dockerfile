FROM golang:1.23-bullseye as builder

COPY . /bridgetest/

ENV CGO_ENABLED=0

#install snowflake
RUN git clone https://gitlab.torproject.org/tpo/anti-censorship/pluggable-transports/snowflake.git \
    && cd snowflake/client && go get -d && go build -ldflags="-s -w" && mv client /usr/bin/snowflake

RUN git clone https://gitlab.torproject.org/tpo/anti-censorship/connectivity-measurement/logcollector.git logCollector \
        && cd logCollector/util && go get -d && go build -ldflags="-s -w" . && mv util /usr/bin/util
        
RUN git clone https://github.com/v2fly/v2ray-core.git \
        && cd v2ray-core \
        && git checkout v5.20.0 \
        && go get -d && go build -ldflags="-s -w" -o v2ray github.com/v2fly/v2ray-core/v5/main && mv v2ray /usr/bin/v2ray

RUN cd /bridgetest/stun-test && go get -d && go build -ldflags="-s -w" -o stun-test

FROM debian:stable-slim

#install python
RUN apt-get update && apt-get install -y wget tcpdump \
    python3 python3-stem tor obfs4proxy curl jq&& apt-get clean

COPY --from=builder /usr/bin/snowflake /usr/bin/snowflake

COPY --from=builder /usr/bin/v2ray /usr/bin/v2ray

COPY --from=builder /bridgetest/ /bridgetest/

COPY --from=builder /usr/bin/util /bridgetest/util

COPY ./docker/entrypoint.sh /entrypoint.sh

RUN chmod +x /entrypoint.sh

RUN ln /usr/bin/tcpdump /usr/sbin/tcpdump

CMD ["/entrypoint.sh"]
